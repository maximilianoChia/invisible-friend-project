const express = require('express');
const cors = require('cors');
const DBconnection = require('./config/db');
const participantsRoute = require('./routes/participantsRoute');

// server
const app = express(); 

// middlewares
app.use(express.json());
app.use(cors());

// DB connection
DBconnection();

app.use('/api/participants', participantsRoute);

app.listen(4000, () => {
    console.log('Server is running on port 4000');
});