// Mongoose ORM
const mongoose = require('mongoose');

require('dotenv').config({ path: 'variables.env' });

const DBurl = process.env.DB_MONGO;

const DBconnection = async () => {
    try {
        await mongoose.connect(DBurl, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log('invisibleFriendGame data base connected');
    } catch (err) {
        console.log(`Data Base ERROR: ${err}`);
        process.exit(1); // Stop the app
    }
}

module.exports = DBconnection;