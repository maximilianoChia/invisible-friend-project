// Routes for participants
const express = require('express');
const router = express.Router();
const participantsController =  require('../controllers/participantsController');

// api/participants
router.post('/', participantsController.createParticipant);
router.get('/', participantsController.getParticipants);
router.put('/:id', participantsController.updateParticipant);
router.delete('/:id', participantsController.deleteParticipant);

module.exports = router;