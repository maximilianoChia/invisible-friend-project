const nodemailer = require("nodemailer");
const emailAccountConfig = require('../config/emailConfig');

 let transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  requireTLS: true,
  auth: {
      user: emailAccountConfig.emailConfig.user,
      pass: emailAccountConfig.emailConfig.password
  }
});

const sendEmail = (participant) => {
  mailOptions = {
    from: emailAccountConfig.emailConfig.user ,
    to: participant.email,
    subject: `¡Tu amigo invisible!`,
    html: ` <h1>¡Hola ${participant.name}!</h2>
            <h2>Tu amigo invisible es: <strong>${participant.invisibleFriend}</strong></h2>`
  };
  transporter.sendMail(mailOptions, (err, info) => {
    if(err)
      console.log(err)
    else
      console.log(info);
  });
}

exports.sendEmail = sendEmail;