const getRandomParticipantIndex = require('./participantsController');
const sendEmail = require('./emailController');

let taken = [];

const matchParticipants = (participant, randomParticipant) => {
    // Check a veces falta un participante - Check taken (Puede estar agregando participante al taken sin asignarle nada) - Puede que tengan que ser más el número de jugadores 
    if (
        participant !== randomParticipant && 
        !taken.includes(randomParticipant) &&
        !participant.firstDiscard.includes(randomParticipant.email) &&
        !participant.secondDiscard.includes(randomParticipant.email)
    ) {
        taken.push(randomParticipant);
        participant.invisibleFriend = randomParticipant.name;
        console.log('============== matchParticipants ==============');
        console.log(participant);
        // sendEmail.sendEmail(participant);
        return
    }
    setTimeout(() => {
        return  matchParticipants(participant, getRandomParticipantIndex.getRandomParticipantIndex());
    }, 0)

}

exports.matchParticipants = matchParticipants;