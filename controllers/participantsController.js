const Participant = require('../models/Participant');
const matchParticipants = require('./gameController');

// Create participant
exports.createParticipant = async (req, res) => {
    try {
        let participant;

        participant = new Participant(req.body); // Se crea el participante según el model. Se empieza a llenar de participantes creados

        await participant.save(); // se guarda el participante en la BD.

        res.send(participant);

    } catch (err) {
        console.log(err);
        res.status(500).send('There was an error');
    }
}


// Get all participants and generate game if generateGame is TRUE 
let participants;

exports.getParticipants = async (req, res) => {
    try {

        participants = await Participant.find();

        res.json(participants);

        if (req.body.generateGame && participants.length >= 6) {

            for (let x = 0; x < participants.length; x++) {
                const p = participants[x];
                const randomParticipant = getRandomParticipantIndex();
                matchParticipants.matchParticipants(p, randomParticipant);
            }

        }

    } catch (err) {
        console.log(err);
        res.status(500).send('There was an error');
    }
}

const getRandomParticipantIndex = () => {
    const randomParticipantIndex = Math.floor(Math.random() * participants.length);
    const randomParticipant = participants[randomParticipantIndex];
    return randomParticipant;
}

// Update participant by id
exports.updateParticipant = async (req, res) => {
    try {

        // Get by destructuring the properties of the request and save them in constants
        const { name, email, firstDiscard, secondDiscard } = req.body;

        let participant = await Participant.findById(req.params.id); // Get the participant by ID obtained in the route param

        if (!participant) {
            res.status(500).json({ msg: 'The participan does not exist.' });
        }

        // Set the new values to the found participant 
        participant.name = name;
        participant.email = email;
        participant.firstDiscard = firstDiscard;
        participant.secondDiscard = secondDiscard;

        participant = await Participant.findOneAndUpdate({ _id: req.params.id}, participant, { new: true });

        // The response sends the participant updated
        res.json(participant);

    } catch (err) {
        console.log(err);
        res.status(500).send('There was an error');
    }
}

// Delete participant by id 
exports.deleteParticipant = async (req, res) => {

    let participant = await Participant.findById(req.params.id); // Get the participant by ID obtained in the route param

    if (!participant) {
        res.status(500).json({ msg: 'The participant does not exist.'})
    }

    await Participant.findOneAndRemove({ _id: req.params.id });

    res.json({ msg: 'The participant has been deleted.'});
}

exports.getRandomParticipantIndex = getRandomParticipantIndex;

