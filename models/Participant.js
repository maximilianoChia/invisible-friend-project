const mongoose = require('mongoose');

const ParticipantSchema = mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true
    },
    firstDiscard: {
        type: String,
        require: true
    },
    secondDiscard: {
        type: String,
        require: true
    },
    invisibleFriend: {
        type: String,
        require: true
    }
});

module.exports = mongoose.model('Participant', ParticipantSchema);